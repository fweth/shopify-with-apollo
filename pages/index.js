import { gql } from "@apollo/client"
import client from "../apollo-client"
import Cart from "../components/cart"
import Product from "../components/product"

export default function Home(props) {
    return (
        <>
            <p>Welcome to {props.name}! These are our products:</p>
            {props.edges.map((edge, k) => (
                <Product key={k} node={edge.node} />
            ))}
            <Cart />
        </>
    )
}

export async function getStaticProps() {
    const {
        data: {
            shop: { name },
        },
    } = await client.query({
        query: gql`
            {
                shop {
                    name
                }
            }
        `,
    })
    const {
        data: {
            products: { edges },
        },
    } = await client.query({
        query: gql`
            {
                products(first: 10) {
                    edges {
                        node {
                            id
                            title
                            ... on Product {
                                variants(first: 10) {
                                    edges {
                                        node {
                                            id
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `,
    })

    return {
        props: {
            name,
            edges,
        },
    }
}
