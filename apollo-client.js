import { ApolloClient, InMemoryCache } from "@apollo/client"

const client = new ApolloClient({
    cache: new InMemoryCache(),
    uri: "https://the-bouche.myshopify.com/api/2021-07/graphql.json",
    headers: {
        "Content-Type": "application/json",
        "X-Shopify-Storefront-Access-Token": "90a1546d86d2790ed472889819b830b8",
    },
})

export default client
