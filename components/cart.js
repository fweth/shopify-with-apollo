import { useEffect } from "react"
import { gql, useMutation, useReactiveVar, useQuery } from "@apollo/client"
import { checkoutIdVar } from "../cache"

const mutationCheckoutCreate = gql`
    mutation checkoutCreate($input: CheckoutCreateInput!) {
        checkoutCreate(input: $input) {
            checkout {
                id
            }
        }
    }
`

const queryNode = gql`
    query ($id: ID!) {
        node(id: $id) {
            id
            ... on Checkout {
                lineItems(first: 250) {
                    edges {
                        node {
                            id
                            title
                            quantity
                        }
                    }
                }
            }
        }
    }
`

export default function Checkout() {
    const checkoutId = useReactiveVar(checkoutIdVar)
    const [createNewCheckout] = useMutation(mutationCheckoutCreate, {
        onCompleted: ({
            checkoutCreate: {
                checkout: { id },
            },
        }) => {
            checkoutIdVar(id)
        },
    })
    const { data } = useQuery(queryNode, {
        variables: { id: checkoutId },
        skip: !checkoutId,
    })
    useEffect(() => {
        if (!checkoutId) {
            createNewCheckout({ variables: { input: {} } })
        }
    }, [])
    return (
        <>
            <p>This is the checkout ID:</p>
            {checkoutId ? (
                <p>{checkoutId}</p>
            ) : (
                <p>Creating new checkout ID...</p>
            )}
            <p>This is your checkout:</p>
            {data ? (
                <>
                    {data.node.lineItems.edges.map((edge) => (
                        <p key={edge.node.id}>
                            title: {edge.node.title}, quantity:{" "}
                            {edge.node.quantity}
                        </p>
                    ))}
                </>
            ) : (
                <p>Creating new checkout...</p>
            )}
        </>
    )
}
