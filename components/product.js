import { useMutation, useReactiveVar, gql } from "@apollo/client"
import { checkoutIdVar } from "../cache"

const mutationCheckoutLineItemsAdd = gql`
    mutation checkoutLineItemsAdd(
        $lineItems: [CheckoutLineItemInput!]!
        $checkoutId: ID!
    ) {
        checkoutLineItemsAdd(lineItems: $lineItems, checkoutId: $checkoutId) {
            checkout {
                id
                lineItems(first: 250) {
                    edges {
                        node {
                            id
                            title
                            quantity
                        }
                    }
                }
            }
        }
    }
`

export default function Product({ node }) {
    const checkoutId = useReactiveVar(checkoutIdVar)
    const [checkoutLineItemsAdd, { loading }] = useMutation(
        mutationCheckoutLineItemsAdd
    )
    return (
        <p
            onClick={() => {
                if (checkoutId) {
                    checkoutLineItemsAdd({
                        variables: {
                            lineItems: [
                                {
                                    variantId: node.variants.edges[0].node.id,
                                    quantity: 1,
                                },
                            ],
                            checkoutId: checkoutId,
                        },
                    })
                }
            }}
        >
            {loading ? "Updating cart..." : node.title}
        </p>
    )
}
